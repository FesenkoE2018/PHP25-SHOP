<?php
include_once 'link.php';

if (!empty($_POST)) {
    $productsName = $_POST['products_name'];
    $productsCode = $_POST['products_code'];
    $productsBrand = $_POST['products_brand'];
    $productsImage = $_POST['products_image'];
    $productsDescription = $_POST['products_description'];
    $productsPrice = $_POST['products_price'];
    $productsCategory = $_POST['products_category'];
    $publish = $_POST['products_publish'];
}

/**
 *  OOP method
 *  PDO module
 *  INSERT INTO TABLE products
 */
if (isset($_POST['submit'])) {
    try {
        $sql = "INSERT INTO `products` (`name`, `articul`, `brand`, `image_path`, `description`, `price`,
            `category_id`) VALUES (?, ?, ?, ?, ?, ?, ?)";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$productsName, $productsCode, $productsBrand, $productsImage, $productsDescription,
            $productsPrice, $productsCategory]);
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }
    header("location: http://blog/admin/");
    exit;
}

/**
 *  OOP method
 *  PDO module
 *  UPDATE TABLE products
 */

if (isset($_POST['edit'])) {
    try {
        $sql = "UPDATE `products` SET `name` = ?, `articul` = ?, `brand` = ?, `image_path` = ?, `description` = ?,
            `price` = ?, `category_id` = ?, `publish` = ? WHERE `id` = " . $_POST['id'];
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$productsName, $productsCode, $productsBrand, $productsImage, $productsDescription,
            $productsPrice, $productsCategory, $publish]);
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }
    header("location: http://blog/admin/");
    exit;
}

/**
 *  OOP method
 *  PDO module
 *  SELECT TABLE products
 */

if (!empty($_GET['id'])) {
    $sql = "SELECT * FROM `products` WHERE id=" . $_GET['id'];;

    try {
        $res = $pdo->query($sql);
        $row = $res->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }
}
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../css/style.css" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
    <script src="../js/script.js"></script>
    <title>Add Admin-Panel</title>
</head>
<body>
<h2 class="title-admin">Addition products panel</h2>
<div class="container">
    <div class="products-create-tablet">
        <h3 class="products-create-tablet__text">Addition products table</h3>
        <form action="products-create.php" method="post">
            <table>
                <tr>
                    <td><label for="products_name">Name:</label></td>
                    <td><input class="products-create__field" type="text" name="products_name"
                               value="<?= (!empty($_GET['id'])) ? $row[0]['name'] : "" ?>" id="products_name"
                               required></td>
                </tr>
                <tr>
                    <td><label for="products_code">Vendor code:</label></td>
                    <td><input class="products-create__field" type="text" name="products_code"
                               value="<?= (!empty($_GET['id'])) ? $row[0]['articul'] : "" ?>" id="products_code"
                               required></td>
                </tr>
                <tr>
                    <td><label for="products_brand">Brand:</label></td>
                    <td><input class="products-create__field" type="text" name="products_brand"
                               value="<?= (!empty($_GET['id'])) ? $row[0]['brand'] : "" ?>" id="products_brand"
                               required></td>
                </tr>
                <tr>
                    <td><label for="products_image">Image_path:</label></td>
                    <td><input class="products-create__field" type="text" name="products_image"
                               value="<?= (!empty($_GET['id'])) ? $row[0]['image_path'] : "" ?>" id="products_image"
                               required</td>
                </tr>
                <tr>
                    <td><label for="products_description">Description:</label></td>
                    <td><input class="products-create__field" type="text" name="products_description"
                               value="<?= (!empty($_GET['id'])) ? $row[0]['description'] : "" ?>"
                               id="products_description" required></td>
                </tr>
                <tr>
                    <td><label for="products_price">Price</label></td>
                    <td><input class="products-create__field" type="number" name="products_price"
                               value="<?= (!empty($_GET['id'])) ? $row[0]['price'] : "" ?>" id="products_price">
                    </td>
                </tr>
                <tr>
                    <td><label for="products_category">Category:</label></td>
                    <td><input class="products-create__field" type="text" name="products_category"
                               value="<?= (!empty($_GET['id'])) ? $row[0]['category_id'] : "" ?>"
                               id="products_category" required></td>
                </tr>
                <tr>
                    <td><label for="products_publish">Publish:</label></td>
                    <td><input class="products-create__field" type="number" min="0" max="1" name="products_publish"
                               value="<?= (!empty($_GET['id'])) ? $row[0]['publish'] : "" ?>"
                               id="products_publish" required></td>
                </tr>
                <input type="hidden" name="id" value="<?= !empty($_GET['id']) ? $_GET['id'] : "" ?>">
            </table>
            <p><input type="submit" name="submit" value="Добавить товар"><input type="submit" name="edit"
                                                                                value="Изменить"></p>
            <p><a href="http://blog/admin/" class="back-admin">back to Admin-Panel</a></p>
        </form>
    </div>
</div>
</body>
</html>
