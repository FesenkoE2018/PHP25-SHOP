<?php
session_start();
require_once('./library/driver.php');
require_once('./library/view.php');
require_once "library/url/request.php";
require_once "library/helper.php";
require_once "library/data.php";
//require_once "library/controller.php";

addRule('/', $routeName, function($params) {
    $articles = findAll();
    $title = "Главная";
    render('home', ['articles' => $articles]);
});

addRule('/article', $routeName, function($params) {
    require_once('./actions/article.php');
});

addRule('/about', 'about', function($params) {
    $title = "О нас";
    render('about');
});



start();

?>
<!doctype html>
<html lang="en">

<!---------- HEAD ---------->
<?php require_once "include/head.php" ?>
<!---------- HEAD ---------->

<body>

<!---------- MENU ---------->
<?php require_once "include/menu.php" ?>
<!---------- MENU ---------->

<!---------- MAIN ---------->
<?= $content ?>
<!---------- MAIN ---------->

<!---------- FOOTER ---------->
<?php require_once "include/footer.php" ?>
<!---------- FOOTER ---------->

</body>
</html>
