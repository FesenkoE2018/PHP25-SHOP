<main class="main">
    <div class="container">
        <?php if (empty($articles)): ?>
            <h2>Статьи не опубликованы</h2>
        <? else : ?>
            <?php foreach ($articles as $article) : ?>
                <div class="main-block">
                    <div class="main-block__text-block">
                        <h2 class="main-block__title"><?= $article['title'] ?></h2>
                        <p class="main-block__text"><?= $article['text'] ?> </p>
                        <a href="http://blog/update/<?= $article['id'] ?>"
                           class="main-block__update">update</a>
                    </div>
                    <div class="main-block__img">
                        <img src="/upload/<?= $article['image'] ?>" alt="" class="main-block__img__size">
                    </div>
                </div>
            <? endforeach; ?>
        <? endif; ?>
    </div>
</main>
