<nav class="menu">
    <div class="menu__container">
        <div class="menu__container__left">
            <a href="" class="menu__container__left__logo">
                <img src="/img/logo.png" alt="" class="menu__container__left__logo__img">
            </a>
            <ul class="menu__container__left__list">
                <li class="menu__container__left__list__item">
                    <a class="menu__container__left__list__item__link <?= ($action == 'home') ? 'active' : '' ?>"
                       href="<?= buildUrl('home') ?>">Home</a>
                </li>
                <li class="menu__container__left__list__item">
                    <a class="menu__container__left__list__item__link <?= ($action == 'about') ? 'active' : '' ?>"
                       href="<?= buildUrl('about') ?>">About</a>
                </li>
                <li class="menu__container__left__list__item">
                    <a class="menu__container__left__list__item__link <?= ($action == 'article' || $action == 'update') ? 'active' : '' ?>"
                       href="<?= buildUrl('article') ?>">Create</a>
                </li>
                <li class="menu__container__left__list__item">
                    <a class="menu__container__left__list__item__link <?= ($action == 'shop') ? 'active' : '' ?>"
                       href="<?= buildUrl('shop') ?>">Shop</a>
                </li>
            </ul>
        </div>
        <div class="menu__container__right">
            <a class="menu__container__right__shop-icon <?= ($action == 'basket') ? 'active' : '' ?>"
               href="<?= buildUrl('basket') ?>"><i class="fas fa-shopping-basket"></i></a>
            <span class="countGoods"><?= ($_SESSION['id']) ? count($_SESSION['id']) : 0 ?></span>
        </div>
    </div>
</nav>