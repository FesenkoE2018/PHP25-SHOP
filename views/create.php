<main class="main">
    <div class="container">
        <div class="block">
            <h2 class="main-block__article">Create new article</h2>
        </div>
        <form enctype="multipart/form-data" action="<?= (!empty($_POST['id']) || $articleID) ? 'update' : 'article'?>" class="create-article" method="post">
            <p class="create-article__row">
                <label for="title" class="create-article__label">Title</label><br>
                <input type="text" class="create-article__title" name="title" placeholder="Enter title"
                       value="<?= ($article) ? $articleTitle : $_POST['title']?>">
            <p class="error"><?= $errors['title'] ?></p>
            </p>
            <p class="create-article__row">
                <label for="text" class="create-article__label">Content</label><br>
                <textarea class="create-article__text" name="text"
                          placeholder="Enter text"><?= ($article) ? $articleText : $_POST['text']?></textarea>
            <p class="error"><?= $errors['text'] ?></p>
            </p>
            <p><input type="file" name="img"></p>
            <p class="error"><?= $errors['img'] ?></p>

            <input type="hidden" name="id" value="<?= ($article) ? $articleID : $_POST['id'] ?>">
            <input type="submit" name="submit" value="Create" class="btn btn__create">
            </form>
    </div>
</main>


