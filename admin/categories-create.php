<?php
include_once 'link.php';

if (!empty($_POST)) {
    $nameCategories = $_POST['name_categories'];
    $descriptionCategories = $_POST['description_categories'];
    $publishCategories = $_POST['publish_categories'];
}

/**
 *  OOP method
 *  PDO module
 *  INSERT INTO TABLE categories
 */
if (isset($_POST['submit'])) {
    try {
        $sql = "INSERT INTO `categories` (`name`, `description`) VALUES (?, ?)";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$nameCategories, $descriptionCategories]);
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }
    header("location: http://blog/admin/");
    exit;
}

/**
 *  OOP method
 *  PDO module
 *  UPDATE TABLE categories
 */

if (isset($_POST['edit'])) {
    try {
        $sql = "UPDATE `categories` SET `name` = ?, `description` = ?, `publish` = ? WHERE `id` = " . $_POST['id'];
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$nameCategories, $descriptionCategories, $publishCategories]);
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }
    header("location: http://blog/admin/");
    exit;
}

/**
 *  OOP method
 *  PDO module
 *  SELECT TABLE categories
 */

if (!empty($_GET['id'])) {
    $sql = "SELECT * FROM `categories` WHERE id=" . $_GET['id'];

    try {
        $res = $pdo->query($sql);
        $row = $res->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }
}
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../css/style.css" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
    <script src="../js/script.js"></script>
    <title>Add Admin-Panel</title>
</head>
<body>
<h2 class="title-admin">Categories panel</h2>
<div class="container">
    <div class="products-create-tablet">
        <h3 class="products-create-tablet__text">Addition category table</h3>
        <form action="categories-create.php" method="post">
            <table>
                <tr>
                    <td><label for="nameCategory">name:</label></td>
                    <td><input class="products-create__field" type="text" name="name_categories"
                               value="<?= (!empty($_GET['id'])) ? $row[0]['name'] : "" ?>" id="nameCategory"
                               required></td>
                </tr>
                <tr>
                    <td><label for="descriptionCategories">description:</label></td>
                    <td><input class="products-create__field" type="text" name="description_categories"
                               value="<?= (!empty($_GET['id'])) ? $row[0]['description'] : "" ?>"
                               id="descriptionCategories"
                               required></td>
                </tr>
                <tr>
                    <td><label for="publishCategories">publish:</label></td>
                    <td><input class="products-create__field" type="number" min="0" max="1" name="publish_categories"
                               value="<?= (!empty($_GET['id'])) ? $row[0]['publish'] : "" ?>" id="publishCategories"
                               required></td>
                </tr>
                <input type="hidden" name="id" value="<?= !empty($_GET['id']) ? $_GET['id'] : "" ?>">
            </table>
            <p><input type="submit" name="submit" value="Add category"><input type="submit" name="edit"
                                                                              value="Edit category"></p>
            <p><a href="http://blog/admin/" class="back-admin">back to Admin-Panel</a></p>
        </form>
    </div>

</div>
</body>
</html>