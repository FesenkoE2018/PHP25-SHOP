<div class="container">
    <div class="goods">
        <?php if (!empty($goods)) : ?>
            <ul class="goods__list">
                <?php foreach ($goods as $good) : ?>
                    <li class="goods__item">
                        <a href="" class="goods__item__link">
                            <img src="../img/i5.jpg" alt="" class="goods__item__img">
                            <p class="goods__item__description">
                                Device type - <?= $good->type ?>
                            </p>
                            <p class="goods__item__description">
                                CPU name - <?= $good->name ?>
                            </p>
                            <p class="goods__item__description">
                                Generation - <?= $good->generation ?>
                            </p>
                            <p class="goods__item__description">
                                Frequency - <?= $good->frequency ?>
                            </p>
                            <p class="goods__item__description">
                                Price - <?= $good->price ?>
                            </p>
                        </a>
                        <a href="http://blog/basket/<?= $good->id; ?>" class="goods__item__link-buy">Add</a>
                    </li>
                <?php endforeach; ?>
            </ul>
        <? else : ?>
            <?= "Something wrong!"; ?>
        <?php endif; ?>
    </div>
</div>