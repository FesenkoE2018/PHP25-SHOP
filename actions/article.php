<?php

//require_once('./library/driver.php');
//require_once('./library/fs.php');

$errors = [];

if ($_FILES['img']['size'] != 0) {
    if ($_FILES['img']['size'] > 40960) {
        $errors['img'] = 'Допустимый размер файла 40Кб';
    }
    if ($_FILES['img']['type'] != 'image/jpeg' && $_FILES['img']['type'] != 'image/png') {
        $errors['img'] = "Недопустимый формат файлов(необходимо jpeg, png)";
    }
}

if (!empty($_POST)) {
    if (empty($_POST['title'])) {
        $errors['title'] = "Поле не может быть пустым";
    }
    if (empty($_POST['text'])) {
        $errors['text'] = "Поле не может быть пустым";
    }
    if (strlen($_POST['title']) > 255) {
        $errors['title'] = "Имя не может быть больше 255 символов";
    }
    if (empty($errors)) {
            $article = $_POST;
            $article['id'] = uniqid();
            if (!empty($_FILES)) {
                $article['image'] = upload($article['id']);
            }
        if (save($article)) {
            header("Location: /");
        }
    }
}

$title = "Create article";
render('article', ['articles' => $articles]);

