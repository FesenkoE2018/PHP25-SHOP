<?php
require_once('./library/driver.php');
require_once('./library/fs.php');
$errors = [];
$article = find($id);
$path = dirname(__DIR__).'/upload/'.$_POST['id'];

if (!empty($article)) {
    $articleTitle = $article['title'];
    $articleText = $article['text'];
    $articleID = $article['id'];
}

if (!empty($_FILES['img'])) {
    if (file_exists($path . '.jpg')) {
        unlink($path . '.jpg');
    }
    if (file_exists($path . '.png')) {
        unlink($path . '.png');
    }
}

if ($_FILES['img']['size'] != 0) {
    if ($_FILES['img']['size'] > 40960) {
        $errors['img'] = 'Допустимый размер файла 40Кб';
    }
    if ($_FILES['img']['type'] != 'image/jpeg' && $_FILES['img']['type'] != 'image/png') {
        $errors['img'] = "Недопустимый формат файлов(необходимо jpeg, png)";
    }
}


if (!empty($_POST)) {
    if (empty($_POST['title'])) {
        $errors['title'] = "Поле не может быть пустым";
    }
    if (empty($_POST['text'])) {
        $errors['text'] = "Поле не может быть пустым";
    }
    if (strlen($_POST['title']) > 255) {
        $errors['title'] = "Имя не может быть больше 255 символов";
    }
    if (empty($errors)) {
        $article = $_POST;
        $article['image'] = upload($article['id']);

        if (save($article)) {
            header("Location: http://blog/home");
        }
    }
}

$page = './views/create.php';
$title = "Create article";
