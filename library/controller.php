<?php
$sid = session_id();

if(!isset($_COOKIE['sid'])) {
    setcookie('sid', $sid);
}

$query = $_SERVER['QUERY_STRING'];
$params = explode('/', $query);
list($action, $id) = $params;
require_once('./library/driver.php');
$page = null;
$title = "";

//$actionFileName = './actions/'.$action.'.php';
//$notFoundFile = './actions/404.php';
//require_once (file_exists($actionFileName)) ? $actionFileName : $notFoundFile;

switch ($action) {
    case '':
        require_once('./actions/home.php');
        break;
    case 'home':
        require_once('./actions/home.php');
        break;
    case 'about':
        require_once('./actions/about.php');
        break;
    case 'article':
        require_once('./actions/article.php');
        break;
    case 'update':
        require_once('./actions/update.php');
        break;
    case 'shop':
        require_once('./actions/shop.php');
        break;
    case 'basket':
        require_once('./actions/basket.php');
        break;
    default:
        require_once('./actions/404.php');
        break;
}

