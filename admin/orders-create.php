<?php
include_once 'link.php';

if (!empty($_POST)) {
    $userID = $_POST['user_id'];
    $userPhone = $_POST['phone'];
}

/**
 *  OOP method
 *  PDO module
 *  INSERT INTO TABLE orders
 */
if (isset($_POST['submit'])) {
    try {
        $sql = "INSERT INTO `orders` (`user_id`, `phone`) VALUES (?, ?)";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$userID, $userPhone]);
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }
    header("location: http://blog/admin/");
    exit;
}

/**
 *  OOP method
 *  PDO module
 *  UPDATE TABLE orders
 */

if (isset($_POST['edit'])) {
    try {
        $sql = "UPDATE `orders` SET `user_id` = ?, `phone` = ? WHERE `id` = " . $_POST['id'];
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$userID, $userPhone]);
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }
    header("location: http://blog/admin/");
    exit;
}

/**
 *  OOP method
 *  PDO module
 *  SELECT TABLE orders
 */

if (!empty($_GET['id'])) {
    $sql = "SELECT * FROM `orders` WHERE id=" . $_GET['id'];

    try {
        $res = $pdo->query($sql);
        $row = $res->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }
}
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../css/style.css" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
    <script src="../js/script.js"></script>
    <title>Add Admin-Panel</title>
</head>
<body>
<h2 class="title-admin">Orders panel</h2>
<div class="container">
    <div class="products-create-tablet">
        <h3 class="products-create-tablet__text">Addition order table</h3>
        <form action="orders-create.php" method="post">
            <table>
                <tr>
                    <td><label for="user_id">user_id:</label></td>
                    <td><input class="products-create__field" type="text" name="user_id"
                               value="<?= (!empty($_GET['id'])) ? $row[0]['user_id'] : "" ?>" id="user_id"
                               required></td>
                </tr>
                <tr>
                    <td><label for="phone">phone:</label></td>
                    <td><input class="products-create__field" type="text" name="phone"
                               value="<?= (!empty($_GET['id'])) ? $row[0]['phone'] : "" ?>" id="phone"
                               required></td>
                </tr>
                <input type="hidden" name="id" value="<?= !empty($_GET['id']) ? $_GET['id'] : "" ?>">
            </table>
            <p><input type="submit" name="submit" value="Add user"><input type="submit" name="edit"
                                                                                value="Edit user"></p>
            <p><a href="http://blog/admin/" class="back-admin">back to Admin-Panel</a></p>
        </form>
    </div>

</div>
</body>
</html>