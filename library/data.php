<?php
$goods = [];

$good = new stdClass();
$good->type ='CPU';
$good->name ='i3';
$good->generation ='Haswell';
$good->frequency ='350 MHz';
$good->price ='$138.00';
$good->id = 1;

$goods[] = $good;

$good = new stdClass();
$good->type ='CPU';
$good->name ='i3';
$good->generation ='Kaby Lake';
$good->frequency ='350 MHz';
$good->price ='$159.00';
$good->id = 2;

$goods[] = $good;

$good = new stdClass();
$good->type ='CPU';
$good->name ='i3';
$good->generation ='Coffee Lake';
$good->frequency ='390 MHz';
$good->price ='$189.00';
$good->id = 3;

$goods[] = $good;

$good = new stdClass();
$good->type ='CPU';
$good->name ='i5';
$good->generation ='Haswell';
$good->frequency ='350 MHz';
$good->price ='$203.00';
$good->id = 4;

$goods[] = $good;

$good = new stdClass();
$good->type ='CPU';
$good->name ='i5';
$good->generation ='Kaby Lake';
$good->frequency ='320 MHz';
$good->price ='$250.00';
$good->id = 5;

$goods[] = $good;

$good = new stdClass();
$good->type ='CPU';
$good->name ='i5';
$good->generation ='Coffee Lake';
$good->frequency ='390 MHz';
$good->price ='$278.00';
$good->id = 6;

$goods[] = $good;

$good = new stdClass();
$good->type ='CPU';
$good->name ='i7';
$good->generation ='Haswell';
$good->frequency ='300 MHz';
$good->price ='$310.00';
$good->id = 7;

$goods[] = $good;

$good = new stdClass();
$good->type ='CPU';
$good->name ='i7';
$good->generation ='Kaby Lake';
$good->frequency ='320 MHz';
$good->price ='$344.00';
$good->id = 8;

$goods[] = $good;

$good = new stdClass();
$good->type ='CPU';
$good->name ='i7';
$good->generation ='Coffee Lake';
$good->frequency ='350 MHz';
$good->price ='$395.00';
$good->id = 9;

$goods[] = $good;

