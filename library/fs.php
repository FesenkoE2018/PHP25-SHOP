<?php
//загрузка файлов

function upload($filename, $path = null)
{

    if (!$path) {
        $path = dirname(__DIR__) . '/upload';
    }
    if (!is_dir($path)) {
        return false;
    }

    $type = $_FILES['img']['type'];
    $tmpPath = $_FILES['img']['tmp_name'];
    $fullName = generateName($filename, $type);
    if (file_exists($tmpPath)) {
        copy($tmpPath, $path.'/'.$fullName);
        return $fullName;
    }
}

function generateName($name, $type)
{
    $map = [
        'image/jpeg' => '.jpg',
        'image/png' => '.png'
    ];

    return $name . $map[$type];
}




