<?php
include_once 'link.php';

/**
 *  PDO module
 *  delete from table categories
 */

if (!empty($_GET['id'])) {
    try {
        $sql = "DELETE FROM `categories` WHERE id = " . $_GET['id'];
        $pdo->query($sql);
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }
    header("location: http://blog/admin/");
    exit;
}