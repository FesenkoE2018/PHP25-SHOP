<div class="container">
    <h2 class="basket">Your goods</h2>
    <ul class="goods__list">
        <? if (isset($_SESSION['id'])) : ?>
            <?php for ($i = 0; $i <= count($_SESSION['id']); $i++) : ?>
                <?php foreach ($goods as $good) : ?>
                    <? if ($_SESSION['id'][$i] == $good->id) : ?>
                        <li class="goods__item">
                            <a href="" class="goods__item__link">
                                <img src="../img/i5.jpg" alt="" class="goods__item__img">
                                <p class="goods__item__description">
                                    Device type - <?= $good->type ?>
                                </p>
                                <p class="goods__item__description">
                                    CPU name - <?= $good->name ?>
                                </p>
                                <p class="goods__item__description">
                                    Generation - <?= $good->generation ?>
                                </p>
                                <p class="goods__item__description">
                                    Frequency - <?= $good->frequency ?>
                                </p>
                                <p class="goods__item__description">
                                    Price - <?= $good->price ?>
                                </p>
                            </a>
                            <a href="http://blog/basket/del/<?= $i ?>">Delete</a>
                        </li>
                    <? endif; ?>
                <?php endforeach; ?>
            <?php endfor; ?>
        <?php endif; ?>
    </ul>
</div>
