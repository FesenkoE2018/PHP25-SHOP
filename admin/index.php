<?php
include_once 'link.php';

$sql = "SELECT * FROM `products`";

if (!empty($_GET['sqlBetween'])) {
    $sql .= $_GET['sqlBetween'];
}

if (!empty($_GET['sqlSearch'])) {
    $sql .= $_GET['sqlSearch'];
}

if (!empty($_GET['sqlPriceOrder'])) {
    $sql .= $_GET['sqlPriceOrder'];
}

try {
    $result = $pdo->query($sql);
    $rowProducts = $result->fetchAll(PDO::FETCH_ASSOC);
}
catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
}


$sql = "SELECT * FROM orders";
$resOrders = mysqli_query($link, $sql);
$rowOrders = mysqli_fetch_all($resOrders, MYSQLI_ASSOC);

$sql = "SELECT * FROM categories";
$resCategories = mysqli_query($link, $sql);
$rowCategories = mysqli_fetch_all($resCategories, MYSQLI_ASSOC);

$sql = "SELECT * FROM users";
$resUsers = mysqli_query($link, $sql);
$rowUsers = mysqli_fetch_all($resUsers, MYSQLI_ASSOC);

$sql = "SELECT * FROM articles";
$resArticles = mysqli_query($link, $sql);
$rowArticles = mysqli_fetch_all($resArticles, MYSQLI_ASSOC);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../css/style.css" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
    <script src="../js/script.js"></script>
    <title>admin</title>
</head>
<body>
<h2 class="title-admin">Welcome to Admin panel</h2>
<div class="container">
    <!----------  PRODUCTS TABLE  ---------->
    <div class="block-products">
        <h3 class="table-caption">Products table</h3>
        <table class="table-products">
            <tr class="table-products__row-th">
                <th class="table-products__row-th_cell">id</th>
                <th class="table-products__row-th_cell">name</th>
                <th class="table-products__row-th_cell">vendor code</th>
                <th class="table-products__row-th_cell">brand</th>
                <th class="table-products__row-th_cell">image_path</th>
                <th class="table-products__row-th_cell">description</th>
                <th class="table-products__row-th_cell">price</th>
                <th class="table-products__row-th_cell">category_id</th>
                <th class="table-products__row-th_cell">created_at</th>
                <th class="table-products__row-th_cell">updated_at</th>
                <th class="table-products__row-th_cell">publish</th>
            </tr>
            <?php foreach ($rowProducts as $items => $item) : ?>
                <tr>
                    <td class="table-products__cell"><?= $item['id'] ?></td>
                    <td class="table-products__cell"><?= $item['name'] ?></td>
                    <td class="table-products__cell"><?= $item['articul'] ?></td>
                    <td class="table-products__cell"><?= $item['brand'] ?></td>
                    <td class="table-products__cell"><?= $item['image_path'] ?></td>
                    <td class="table-products__cell"><?= $item['description'] ?></td>
                    <td class="table-products__cell"><?= $item['price'] ?></td>
                    <td class="table-products__cell"><?= $item['category_id'] ?></td>
                    <td class="table-products__cell"><?= $item['created_at'] ?></td>
                    <td class="table-products__cell"><?= $item['updated_at'] ?></td>
                    <td class="table-products__cell"><?= $item['publish'] ?></td>
                    <td class="table-products__cell_btn"><a
                                href="http://blog/admin/products-create.php?id=<?= $item['id'] ?>"
                                class="btn-adm btn-adm_edit">edit </a></td>
                    <td class="table-products__cell_btn"><a
                                href="http://blog/admin/products-delete.php?id=<?= $item['id'] ?>"
                                class="btn-adm btn-adm_delete">delete</a></td>
                </tr>
            <?php endforeach ?>
        </table>
    </div>
    <a href="products-create.php" class="products-create_btn">Add</a>

    <!----------  PRODUCTS FILTER  ---------->
    <h4 class="products-filter">Products table filter</h4>
    <form action="products-filter.php" method="post">
        <p>By price order
            <select name="price_filter" id="">
                <option value=""></option>
                <option value="ascending">ascending</option>
                <option value="descending">descending</option>
            </select>
            <span class="input-distance"></span>
            Price
            from: <input type="number" min="0" max="99999" name="price_between[]">
            to: <input type="number" min="0" max="99999" name="price_between[]">
            <span class="input-distance"></span>
            Search: <input type="text" name="search">
        </p>
        <p><input type="submit" value="Filter" name="products_filter"></p>
    </form>

    <!----------  ORDERS TABLE  ---------->
    <div class="block-products">
        <h3 class="table-caption">Orders table</h3>
        <table class="table-products">
            <tr class="table-products__row-th">
                <th class="table-products__row-th_cell">id</th>
                <th class="table-products__row-th_cell">user_id</th>
                <th class="table-products__row-th_cell">phone</th>
                <th class="table-products__row-th_cell">created_at</th>
                <th class="table-products__row-th_cell">updated_at</th>
            </tr>
            <?php foreach ($rowOrders as $items => $item) : ?>
                <tr>
                    <td class="table-products__cell"><?= $item['id'] ?></td>
                    <td class="table-products__cell"><?= $item['user_id'] ?></td>
                    <td class="table-products__cell"><?= $item['phone'] ?></td>
                    <td class="table-products__cell"><?= $item['created_at'] ?></td>
                    <td class="table-products__cell"><?= $item['updated_at'] ?></td>
                    <td class="table-products__cell_btn"><a
                                href="http://blog/admin/orders-create.php?id=<?= $item['id'] ?>"
                                class="btn-adm btn-adm_edit">edit </a></td>
                    <td class="table-products__cell_btn"><a
                                href="http://blog/admin/orders-delete.php?id=<?= $item['id'] ?>"
                                class="btn-adm btn-adm_delete">delete</a></td>
                </tr>
            <?php endforeach ?>
        </table>
    </div>
    <a href="orders-create.php" class="products-create_btn">Add</a>

    <!----------  CATEGORIES TABLE  ---------->
    <div class="block-products">
        <h3 class="table-caption">Categories table</h3>
        <table class="table-products">
            <tr class="table-products__row-th">
                <th class="table-products__row-th_cell">id</th>
                <th class="table-products__row-th_cell">name</th>
                <th class="table-products__row-th_cell">description</th>
                <th class="table-products__row-th_cell">publish</th>
            </tr>
            <?php foreach ($rowCategories as $items => $item) : ?>
                <tr>
                    <td class="table-products__cell"><?= $item['id'] ?></td>
                    <td class="table-products__cell"><?= $item['name'] ?></td>
                    <td class="table-products__cell"><?= $item['description'] ?></td>
                    <td class="table-products__cell"><?= $item['publish'] ?></td>
                    <td class="table-products__cell_btn"><a
                                href="http://blog/admin/categories-create.php?id=<?= $item['id'] ?>"
                                class="btn-adm btn-adm_edit">edit </a></td>
                    <td class="table-products__cell_btn"><a
                                href="http://blog/admin/categories-delete.php?id=<?= $item['id'] ?>"
                                class="btn-adm btn-adm_delete">delete</a></td>
                </tr>
            <?php endforeach ?>
        </table>
    </div>
    <a href="categories-create.php" class="products-create_btn">Add</a>

    <!----------  USERS TABLE  ---------->
    <div class="block-products">
        <h3 class="table-caption">Users table</h3>
        <table class="table-products">
            <tr class="table-products__row-th">
                <th class="table-products__row-th_cell">id</th>
                <th class="table-products__row-th_cell">name</th>
                <th class="table-products__row-th_cell">email</th>
                <th class="table-products__row-th_cell">phone</th>
                <th class="table-products__row-th_cell">password</th>
                <th class="table-products__row-th_cell">created_at</th>
                <th class="table-products__row-th_cell">updated_at</th>
            </tr>
            <?php foreach ($rowUsers as $items => $item) : ?>
                <tr>
                    <td class="table-products__cell"><?= $item['id'] ?></td>
                    <td class="table-products__cell"><?= $item['name'] ?></td>
                    <td class="table-products__cell"><?= $item['email'] ?></td>
                    <td class="table-products__cell"><?= $item['phone'] ?></td>
                    <td class="table-products__cell"><?= $item['password'] ?></td>
                    <td class="table-products__cell"><?= $item['created_at'] ?></td>
                    <td class="table-products__cell"><?= $item['updated_at'] ?></td>
                    <td class="table-products__cell_btn"><a
                                href="http://blog/admin/users-create.php?id=<?= $item['id'] ?>"
                                class="btn-adm btn-adm_edit">edit </a></td>
                    <td class="table-products__cell_btn"><a
                                href="http://blog/admin/users-delete.php?id=<?= $item['id'] ?>"
                                class="btn-adm btn-adm_delete">delete</a></td>
                </tr>
            <?php endforeach ?>
        </table>
    </div>
    <a href="users-create.php" class="products-create_btn">Add</a>

    <!----------  ARTICLES TABLE  ---------->
    <div class="block-products">
        <h3 class="table-caption">Articles table</h3>
        <table class="table-products">
            <tr class="table-products__row-th">
                <th class="table-products__row-th_cell">id</th>
                <th class="table-products__row-th_cell">title</th>
                <th class="table-products__row-th_cell">description</th>
                <th class="table-products__row-th_cell">created_at</th>
                <th class="table-products__row-th_cell">updated_at</th>
            </tr>
            <?php foreach ($rowArticles as $items => $item) : ?>
                <tr>
                    <td class="table-products__cell"><?= $item['id'] ?></td>
                    <td class="table-products__cell"><?= $item['title'] ?></td>
                    <td class="table-products__cell"><?= $item['description'] ?></td>
                    <td class="table-products__cell"><?= $item['created_at'] ?></td>
                    <td class="table-products__cell"><?= $item['updated_at'] ?></td>
                    <td class="table-products__cell_btn"><a
                                href="http://blog/admin/articles-create.php?id=<?= $item['id'] ?>"
                                class="btn-adm btn-adm_edit">edit </a></td>
                    <td class="table-products__cell_btn"><a
                                href="http://blog/admin/articles-delete.php?id=<?= $item['id'] ?>"
                                class="btn-adm btn-adm_delete">delete</a></td>
                </tr>
            <?php endforeach ?>
        </table>
    </div>
    <a href="articles-create.php" class="products-create_btn">Add</a>
</div>
</body>
</html>